def parseOp(operand):
    if operand.type == idaapi.o_displ or operand.type == idaapi.o_phrase:
        if operand.reg == 4: #esp/rsp
            return True, operand.addr
    return False, 0

def convert_operand(operand, size, num, pos):
    if operand.type == idaapi.o_displ or operand.type == idaapi.o_phrase:
        if operand.reg == 4: #esp/rsp
            print "Got op size %d" % (operand.addr)
            if operand.addr < size:
                print "%x" % pos
                OpHex(pos, num)
            

def calc_args_size(ea):
    f = idaapi.get_func(ea)
    print "%x" % ea
    frsize = GetFrameLvarSize(ea)
    position = f.startEA
    size = 0
    while position < f.endEA:
        instr = idautils.DecodeInstruction(position)
        if instr is None:
            print "%x: Not and instruction found" % position
            break
        mnem = instr.get_canon_mnem()
        if mnem == "mov":
            op1 = instr.Op1
            op2 = instr.Op2
            is_ok, val = parseOp(op1)
            if is_ok == True:
                if val > size:
                    size = val
            is_ok, val = parseOp(op2)
            if is_ok == True:
                if val > size:
                    size = val
        position += instr.size
    print size - frsize
    return size - frsize

def convert_frame(ea):
    f = idaapi.get_func(ea)
    frsize = GetFrameLvarSize(ea)
    position = f.startEA
    size = 0
    while position < f.endEA:
        instr = idautils.DecodeInstruction(position)
        if instr is None:
            print "%x: Not and instruction found" % position
            break
        mnem = instr.get_canon_mnem()
        if mnem == "call":
            if instr.Op1.addr != 0:
                size_f = calc_args_size(instr.Op1.addr)
                if size_f > size:
                    size = size_f
        position += instr.size
    print size
    position = f.startEA
    while position < f.endEA:
        instr = idautils.DecodeInstruction(position)
        if instr is None:
            print "%x: Not and instruction found" % position
            break
        mnem = instr.get_canon_mnem()
        #if mnem == "mov":
        op1 = instr.Op1
        op2 = instr.Op2
        convert_operand(op1, size, 0, position)
        convert_operand(op2, size, 1, position)
        position += instr.size